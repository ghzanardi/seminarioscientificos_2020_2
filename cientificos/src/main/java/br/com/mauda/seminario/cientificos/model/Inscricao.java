package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private Estudante estudante;
    private Seminario seminario;
    private SituacaoInscricaoEnum situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        seminario.adcionarInscricao(this);
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.estudante = estudante;
        this.direitoMaterial = direitoMaterial;
        this.estudante.adicionarInscricao(this);
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.COMPRADO;

    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;
    }

    public void realizarCheckIn() {
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.CHECKIN;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacaoInscricaoEnum;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

}